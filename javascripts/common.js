$(document).ready(function() {

// Edit profile for mobile

	$('.profile-info__details-icon_mobile').click(function() {
		$(this).fadeOut();
		$('.profile-info__buttons-mobile').fadeIn();
		$('.profile-info__details').fadeOut();
		$('.profile-edit').fadeIn();
	})

	$('.profile-info__links').click(function() {
		$('.profile-info__buttons-mobile').fadeOut();
		$('.profile-info__details').fadeIn();
		$('.profile-edit').fadeOut();
		$('.profile-info__details-icon_mobile').fadeIn();
		$('.popover-markup').fadeOut();
	})
	// Navigation
	$('.profile__nav-link_settings').click(function() {
		$('.profile-info__wrap').fadeOut();
		$('.profile-info__settings').fadeIn();
	})
	$('.profile__nav-link').click(function() {
		$('.profile__nav-link').removeClass('profile__nav-link_active');
		$(this).addClass('profile__nav-link_active');
	})
	$('.profile__nav-link_about').click(function() {
		$('.profile-info__wrap').fadeIn();
		$('.profile-info__settings').fadeOut();
	})
	//form
	$('.profile-info__setting-input, .login__input, .profile-edit__input').focus(function() {
		$('.profile-info__setting-input, .login__input, .profile-edit__input').css('border-bottom', '1px solid #d0dce7');
		$('.login__label, .profile-edit__label').css('color', '#a6b0b7');
		$(this).prev().css('color', '#007ee5');
		$(this).css('border-bottom', '2px solid #007ee5');
	})
	// Setting title content
	 $(window).resize(function() {
	  if ($(window).width() < 767) {
	    $('.profile-info__title_settings').html('Settings');
	  }
	 else {
	    $('.profile-info__title_settings').html('Change Password');
	 }
	});



});